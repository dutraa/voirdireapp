
import shutil
import numpy as np
import tensorflow as tf


# Determine CSV, label, and key columns
CSV_COLUMNS = 'country,designation,province,region_one,region_two,taster_name,taster_twitter_handle,title,variety,winery,price,points,key'.split(',')
LABEL_COLUMN = 'points'
KEY_COLUMN = 'key'

# Set default values for each CSV column
DEFAULTS = [[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0], ['nokey']]
TRAIN_STEPS = 1000


# Create an input function reading a file using the Dataset API
# Then provide the results to the Estimator API
def read_dataset(filename, mode, batch_size = 512):
  def _input_fn():
    def decode_csv(value_column):
      columns = tf.decode_csv(value_column, record_defaults=DEFAULTS)
      features = dict(zip(CSV_COLUMNS, columns))
      label = features.pop(LABEL_COLUMN)
      return features, label
    
    # Create list of files that match pattern
    file_list = tf.gfile.Glob(filename)

    # Create dataset from file list
    dataset = (tf.data.TextLineDataset(file_list)  # Read text file
                 .map(decode_csv))  # Transform each elem by applying decode_csv fn
      
    if mode == tf.estimator.ModeKeys.TRAIN:
        num_epochs = None # indefinitely
        dataset = dataset.shuffle(buffer_size=10*batch_size)
    else:
        num_epochs = 1 # end-of-input after this
 
    dataset = dataset.repeat(num_epochs).batch(batch_size)
    return dataset
  return _input_fn

# Define feature columns
def get_categorical(name, values):
  return tf.feature_column.indicator_column(
    tf.feature_column.categorical_column_with_vocabulary_list(name, values))

def get_cols():
  # Define column types
  return [\
          tf.feature_column.numeric_column('country'),
          tf.feature_column.numeric_column('designation'),
          tf.feature_column.numeric_column('province'),
          tf.feature_column.numeric_column('region_one'),
          tf.feature_column.numeric_column('region_two'),
          tf.feature_column.numeric_column('taster_name'),
          tf.feature_column.numeric_column('taster_twitter_handle'),
          tf.feature_column.numeric_column('title'),
          tf.feature_column.numeric_column('variety'),
          tf.feature_column.numeric_column('winery'),
          tf.feature_column.numeric_column('price')
      ] 


# Create serving input function to be able to serve predictions later using provided inputs
def serving_input_fn():
    feature_placeholders = {
        'country': tf.placeholder(tf.float32, [None]),
        'designation': tf.placeholder(tf.float32, [None]),
        'province': tf.placeholder(tf.float32, [None]),
        'region_one': tf.placeholder(tf.float32, [None]),
        'region_two': tf.placeholder(tf.float32, [None]),
        'taster_name': tf.placeholder(tf.float32, [None]),
        'taster_twitter_handle': tf.placeholder(tf.float32, [None]),
        'title': tf.placeholder(tf.float32, [None]),
        'variety': tf.placeholder(tf.float32, [None]),
        'winery': tf.placeholder(tf.float32, [None]),
        'price': tf.placeholder(tf.float32, [None]),
    }
    features = {
        key: tf.expand_dims(tensor, -1)
        for key, tensor in feature_placeholders.items()
    }
    return tf.estimator.export.ServingInputReceiver(features, feature_placeholders)

# Create estimator to train and evaluate
def train_and_evaluate(output_dir):
  EVAL_INTERVAL = 300
  run_config = tf.estimator.RunConfig(save_checkpoints_secs = EVAL_INTERVAL,
                                      keep_checkpoint_max = 3)
  estimator = tf.estimator.DNNRegressor(
                       model_dir = output_dir,
                       feature_columns = get_cols(),
                       hidden_units = [64, 32],
                       config = run_config)
  train_spec = tf.estimator.TrainSpec(
                       input_fn = read_dataset('train.csv', mode = tf.estimator.ModeKeys.TRAIN),
                       max_steps = TRAIN_STEPS)
  exporter = tf.estimator.LatestExporter('exporter', serving_input_fn)
  eval_spec = tf.estimator.EvalSpec(
                       input_fn = read_dataset('eval.csv', mode = tf.estimator.ModeKeys.EVAL),
                       steps = None,
                       start_delay_secs = 60, # start evaluating after N seconds
                       throttle_secs = EVAL_INTERVAL,  # evaluate every N seconds
                       exporters = exporter)
  tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)


# Run the model
shutil.rmtree('points_trained', ignore_errors = True) # start fresh each time
train_and_evaluate('points_trained')  